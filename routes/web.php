<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * Le but est de faire un blog avec laravel
 */

//blog
Route::get('/blog/posts', 'Blog\PostController@index')->name('blog.index');
Route::get('/blog/post/{slug}/{id}', 'Blog\PostController@post')->name('blog.post');
Route::get('/blog/tag/{id}', 'Blog\TagController@tag')->name('blog.tag');

Route::get('/user/{id}', 'UserController@user')->name('user.user');

//auth
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

//Admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function (){

	Route::get('/salut', function () {
		return 'hello world with lavarel!';
	});

});