<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title', 'content', 'slug'
	];

	/**
	 * Get all of the tags for the post.
	 */
	public function tags()
	{
		return $this->morphToMany('App\Tags', 'taggables');
	}

	/**
	 * Get the user that owns the post.
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
