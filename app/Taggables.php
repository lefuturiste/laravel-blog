<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taggables extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = false;
}
