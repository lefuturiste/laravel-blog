<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title'
	];

	/**
	 * Get all of the posts that are assigned this tag.
	 */
	public function posts()
	{
		return $this->morphedByMany('App\Posts', 'taggables');
	}
}
