<?php

namespace App\Http\Controllers\Blog;

use App\Posts;
use App\Tags;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function tag($id)
	{
		$tag = Tags::findOrFail($id);
		return view('blog.tag', [
			'tag' => $tag,
			'posts' => $tag->posts
		]);
	}
}
