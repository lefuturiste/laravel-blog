<?php

namespace App\Http\Controllers\Blog;

use App\Posts;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use League\Flysystem\Adapter\Local;
use Symfony\Component\Debug\Debug;

class PostController extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	public function index()
	{
		$posts = Posts::paginate(5);
		return view('blog.index', [
			'posts' => $posts
		]);
	}

	public function post($slug, $id)
	{
		$post = Posts::findOrFail($id);
		return view('blog.post', [
			'post' => $post
		]);
	}
}
