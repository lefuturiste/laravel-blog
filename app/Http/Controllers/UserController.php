<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function user($id)
	{
		$user = User::findOrFail($id);
		$posts = $user->posts;
		return view('user.user', [
			'user' => $user,
			'posts' => $posts
		]);
	}
}
