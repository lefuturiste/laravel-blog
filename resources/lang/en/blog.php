<?php

return [
	'all_posts' => 'All posts',
	'all_tags' => 'All Tags',
	'read_me' => 'Read me',
	'post_with_tag' => 'Posts with this tag',
	'post_owned_by_this_user' => 'Post with this user'
];