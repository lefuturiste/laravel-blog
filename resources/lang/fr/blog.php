<?php

return [
	'all_posts' => 'Tous les posts',
	'all_tags' => 'Tous les tags',
	'read_me' => 'Lisez moi',
	'post_with_tag' => 'Les posts associés à ce tag',
	'post_owned_by_this_user' => 'Les posts écrits par cette utilisateur'
];