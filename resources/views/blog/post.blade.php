@extends('layouts.master')
@section('title', $post->title)
@section('content')
    <p>Dans le(s) tags :
        <ul>
            @foreach($post->tags as $tag)
                <li><a href="{{ route('blog.tag', ['id' => $tag->id]) }}">{{ $tag->title }}</a></li>
            @endforeach
        </ul>
    </p>
    <p>Autheur : <a href="{{ route('user.user', ['id' => $post->user->id]) }}">{{ $post->user->name }}</a></p>
    {!! Markdown::convertToHtml($post->content) !!}
@endsection