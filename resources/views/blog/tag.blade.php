@extends('layouts.master')
@section('title', $tag->title)
@section('content')
    <h2>@lang('blog.post_with_tag')</h2>
    @include('blog.posts')
@endsection