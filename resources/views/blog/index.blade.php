@extends('layouts.master')
@section('title', __('blog.all_posts'))
@section('content')
    @include('blog.posts')

    {{-- Include pagination --}}
    {{ $posts->links() }}
@endsection