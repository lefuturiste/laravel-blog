<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead>
        <tr>
            <th>Title</th>
            <th>Published at</th>
            <th>Author</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->created_at->format('F, j Y H:i') }}</td>
                <td><a href="{{ route('user.user', ['id' => $post->user->id]) }}">{{ $post->user->name }}</a></td>
                <td><a class="btn btn-primary" href="{{ route('blog.post', ['id' => $post->id, 'slug' => $post->slug]) }}">
                        @lang('blog.read_me')
                    </a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>