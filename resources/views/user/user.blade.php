@extends('layouts.master')
@section('title', $user->name)
@section('content')
    <h2>@lang('blog.post_owned_by_this_user')</h2>
    @include('blog.posts')
@endsection