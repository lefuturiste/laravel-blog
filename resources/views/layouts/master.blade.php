<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    <div class="col-md-9">
        <h1>@yield('title')</h1>

        @yield('content')
    </div>
    <div class="col-md-3">
        <h2>@lang('blog.all_tags')</h2>
        <ul>
            @foreach(\App\Tags::all() as $tag)
                <li><a href="{{ route('blog.tag', ['id' => $tag->id]) }}">{{ $tag->title }}</a></li>
            @endforeach
        </ul>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
