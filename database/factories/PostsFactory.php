<?php
//define a model factory (for seeding) for App\Posts Model
$factory->define(App\Posts::class, function (Faker\Generator $faker) {
	return [
		'title' => $faker->sentence(4),
		'content' => $faker->text(2500),
		'slug' => $faker->slug(),
		'user_id' => function () {
			return factory(App\User::class)->create()->id;
		},
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime
	];
});
