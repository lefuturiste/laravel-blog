<?php
//define a model factory (for seeding) for App\Tags Model
$factory->define(App\Tags::class, function (Faker\Generator $faker) {
	return [
		'title' => $faker->sentence(4),
		'created_at' => $faker->dateTime,
		'updated_at' => $faker->dateTime
	];
});
