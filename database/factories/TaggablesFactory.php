<?php
//define a model factory (for seeding) for App\Posts Model
$factory->define(App\Taggables::class, function (Faker\Generator $faker) {
	return [
		'tags_id' => function () {
			return factory(App\Tags::class)->create()->id;
		},
		'taggables_type' => \App\Posts::class,
		'taggables_id' => function () {
			return factory(App\Posts::class)->create()->id;
		}
	];
});
