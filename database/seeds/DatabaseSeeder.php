<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(TaggablesTableSeeder::class);
//		$this->call(TagsTableSeeder::class);
//		$this->call(UserTableSeeder::class);
    }
}
