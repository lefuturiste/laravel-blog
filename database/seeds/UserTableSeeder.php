<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//get model of seeding database/factories/PostsFactory/ and create 10 row
		factory(App\User::class, 3)->create();
	}
}
