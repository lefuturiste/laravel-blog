<?php

use Illuminate\Database\Seeder;

class TaggablesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//get model of seeding database/factories/PostsFactory/ and create 10 row
		factory(App\Taggables::class, 10)->create();
	}
}
