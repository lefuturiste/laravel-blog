<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//get model of seeding database/factories/PostsFactory/ and create 10 row
		factory(App\Posts::class, 10)->create();
    }
}
