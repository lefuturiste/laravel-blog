<?php
/**
 * Laravel Debug bar config
 * https://github.com/barryvdh/laravel-debugbar
 *
 */
return [

	/**
	 * Enable the debug bar
	 *
	 * The profiler is enabled by default, if you have app.debug=true.
	 * You can override that in the config (debugbar.enabled). See more options in config/debugbar.php
	 */
	'enabled' => false
];